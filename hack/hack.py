MAX_CODE = 1000000000

def main():
    code = int(input('Entrez le code secret pour accéder au bonus caché: '))
    if check_secret_code(code):
        print('Accès autorisé !')
        print('Victoire instantanée activée !')
    else:
        print('Code incorrect.')

def check_secret_code(code):
    if code >= MAX_CODE:
        return False
    keys = [(code // 10**n) % 10 for n in range(0xa-1)]
    tic = 5*keys[5] == 141 - 12*keys[6] and goat_poop(keys[2]) == 0xcaca
    tac = keys[3] == keys[0] and 11*keys[5] + 3*keys[6] == 123
    toc = keys[7] == 1 << 1 << 1 and keys[1] == 4 and keys[keys[6]] == keys[2]
    if tic and tac and toc:
        return not (bool(keys[4]) or magic(code, 7) != 2)
    return False

def goat_poop(x):
    dalmatiens = 101
    xdalmatiens = 0x101
    return dalmatiens*x*xdalmatiens

def magic(x, n):
    if x < 10:
        return int(x - n == 0)
    return magic(x%10, n) + magic(x//10, n)


main()
