import random
import sys

def encrypt(text, key):
    output = []
    for l in text:
        if l.isalpha():
            x = ord(l.lower()) - ord('a')
            e = key[x]
            if l.isupper():
                e = e.upper()
            output.append(e)
        elif l == ' ':
            n = random.randint(1, 9)
            for _ in range(n):
                output.append(random.choice(key))
            output.append(str(n))
        else:
            if l.isdigit():
                output.append('|')
            output.append(l)
    return ''.join(output)

def decrypt(text, key):
    output = []
    level = 0
    for l in text:
        if l.isalpha():
            i = key.index(l.lower())
            if l.isupper():
                d = chr(i + ord('A'))
            else:
                d = chr(i + ord('a'))
            output.append(d)
        elif l.isdigit():
            if level == 0:
                n = int(l)
                for _ in range(n):
                    output.pop()
                output.append(' ')
            else:
                output.append(l)
                level -= 1
        elif l == '|':
            level += 1
        else:
            output.append(l)
    return ''.join(output)


def encrypt_file(input_file, output_file, key):
    with open(input_file, 'r') as in_file:
        text = in_file.read()
    text = encrypt(text, key)
    with open(output_file, 'w') as output_file:
        output_file.write(text)

def decrypt_file(input_file, output_file, key):
    with open(input_file, 'r') as in_file:
        text = in_file.read()
    text = decrypt(text, key)
    with open(output_file, 'w') as output_file:
        output_file.write(text)

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print('Invalid command')
        print('To encrypt, use:')
        print('python secret.py encrypt <input_file> <output_file> <key>')
        print()
        print('To decrypt, use:')
        print('python secret.py decrypt <input_file> <output_file> <key>')
        quit()
    if sys.argv[1] == 'encrypt':
        encrypt_file(sys.argv[2], sys.argv[3], sys.argv[4])
    elif sys.argv[1] == 'decrypt':
        decrypt_file(sys.argv[2], sys.argv[3], sys.argv[4])
    else:
        print('Invalid command. Should be encrypt or decrypt')
